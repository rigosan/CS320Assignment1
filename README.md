Sergio Santana
rigosanle@gmail.com

prog1_1.c contains a main() function that prompts the user to input a name. It then prints a greeting "Hello Firsty Mclasty!"

prog1_2.h is a header file containing the struct STACK and the header        functions that will be implemented in prog1_2.c

prog1_2.c implements the STACK with Push, Pop and Grow. One important note is 
that when the Stack is empty Pop returns a -1.

prog1_3.c contains a main() function that prompts the user for either a "pop"
or a "push int" on a line. Int being an integer. All other inputs are ignored. Needless to say if "pop" is inputted it Pops the stack and prints it. "push int" Pushes the value onto the stack, if max capacity is reached Grow the stack.

