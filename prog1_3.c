#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "prog1_2.h"

int getCount(char *ptr);

int main(int argc, char *argv[]){
    printf("Assignment #1-3, Sergio Santana, rigosanle@gmail.com\n");
    int N=0;
    int i;
   
    //checks that argument count is 1( excluding program)
    //if not exit program
    if(argc!=2){
    printf("\nThis program expects a single command line argument.\n");
    exit(0);
    }
    
    N = atoi(argv[1]);
    STACK *s;
    s=MakeStack(N);
    //allocate enough for 256 characters
    char *data=(char*) malloc(sizeof(char)*256);
    
    //loop that prompts user N times
    for(i=0;i<N;i++){
        int count=0;
        printf(">");
        fgets(data,256,stdin);
        count=getCount(data);
        data=strtok(data," ");
        
        //if one token and that token is pop then Pop value
        if(count==1 && strstr(data,"pop")!=NULL){
        
        printf("%d\n",Pop(s));

        }
        //if exactly two tokens and first token push then Push value
        else if(count==2 && strstr(data,"push" )!=NULL){
        data=strtok(NULL," ");
        int num=atoi(data);
        Push(s,num);


    }
  }
    return 0;   
}

//method that returns token number
int getCount(char *ptr){
    int count=0;
    int getOut=0;
    while(*ptr){
        if(*ptr==' '|| *ptr=='\n' || *ptr=='\t')
            getOut=0;
        else if (getOut==0){
            getOut=1;
            count++;
        }
        ptr++;
    }
            
    return count;
}


