#include "prog1_2.h"
#include <stdlib.h>

//make Stack pointer, allocate mem on heap, set size=0, set capacity
STACK* MakeStack(int initialCapacity){
    STACK *S;
    S=(STACK*) malloc(1*sizeof(STACK));
    S->data=(int*) malloc(initialCapacity*sizeof(int));
    S->size=0; 
    S->capacity=initialCapacity;
    return S; 
}
//push int onto stack, if full call grow 
void Push(STACK *stackPtr,int data){
    if(stackPtr->size==stackPtr->capacity){
        Grow(stackPtr);
    }
    stackPtr->data[stackPtr->size]=data;
    stackPtr->size++;
}
//pop off stack and subtract size by 1
int Pop(STACK *stackPtr){
    if(stackPtr->size==0) {
        return -1;
    }
    int toPop;
    toPop=stackPtr->data[stackPtr->size-1];
    stackPtr->size--;
    return toPop;
}

//Grow doubles stack capacity by using realloc
void Grow(STACK *stackPtr){
    //# of bytes to reallocate
    int numBytes;
    //multiply capacity by 2
    stackPtr->capacity=stackPtr->capacity<<1;
    numBytes=stackPtr->capacity*sizeof(int); 
    stackPtr->data=(int*) realloc(stackPtr->data,numBytes);
}

